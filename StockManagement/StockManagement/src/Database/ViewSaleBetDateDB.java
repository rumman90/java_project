package Database;

import Model.ViewSaleBetDateModel;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ViewSaleBetDateDB extends ConnectDB {

    public ArrayList<ViewSaleBetDateModel> ViewSalesBetweenDate(ViewSaleBetDateModel aViewSale) {
        ArrayList<ViewSaleBetDateModel> aViewSaleBetDate = new ArrayList<ViewSaleBetDateModel>();
        try {
            Query = "SELECT s.Itemid, S.StockOutQuantity,p.ProductName FROM tblstockout S \n"
                    + "INNER JOIN tblproduct p ON s.Itemid=p.ProductId Where s.date BETWEEN '"+aViewSale.formDate+"' AND '"+aViewSale.toDate+"' ";
            ResultSet rs = stmt.executeQuery(Query);
            while (rs.next()) {
                ViewSaleBetDateModel ViewSales = new ViewSaleBetDateModel();
                ViewSales.ProductName = rs.getString("ProductName");
                ViewSales.StockOutQuantity = rs.getInt("StockOutQuantity");
                aViewSaleBetDate.add(ViewSales);
            }

            conn.close();
        } catch (Exception e) {
            //
        }
        return aViewSaleBetDate;
    }
}
