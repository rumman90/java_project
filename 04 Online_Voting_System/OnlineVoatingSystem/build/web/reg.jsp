
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/home.css" rel="stylesheet" type="text/css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="header.jsp" %>
        <title>JSP Page</title>

        <style>    
            #registrationForm label.error {
                color: red;
                font-style: italic;
            }

        </style>

    </head>
    <body>
        <%  
            String  message = request.getParameter("message");
           
        %>

        <div class="back_image">

            <div class="reg" style="overflow-y:scroll; height:390px; background:#ffffff">

                <pre>
            <h3 style="margin-left: 100px; margin-bottom: -50px;margin-top: -15px;">New Voter Register Here</h3><br><br>

            <h3 style="margin-left: 100px; margin-bottom: -50px;margin-top: -15px;color:red"><%  if ( message!= null) {
               out.print(message);
               request.setAttribute("message", null); 
            }%></h3>

            <form action="Register" method="get" id="registrationForm">
            <input name="name" id="name" placeholder="First Name" type="text">
                                
            <input name="surname"  placeholder="Last Name" type="text">
            
            <input name="voter_card_number"  placeholder="national ID Number" type="number">
            
            <input name="contact"  placeholder="Contact Number" type="number"> 
            
            <input name="email"  placeholder="Mail ID" type="email"> 
            
            <input name="address"  placeholder="Native Address" type="text">
                     
            <input name="dob"  placeholder="Date Of Birth" type="date"> 
            

                        
                    <input value="Register"   type="submit" class="btn" style="padding-bottom: 30px; width: 150px;"> 
            </form>
                </pre>

            </div>
        </div>



        <script src="Assets/Js/jquery-3.2.1.js" type="text/javascript"></script>
        <script src="Assets/Js/jquery.validate.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $("#registrationForm").validate({
                    rules: {
                        name: "required",
                        voter_card_number: {
                            required: true,
                            minlength: 5
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        contact: "required"
                    },
                    messages: {
                        name: "\n\t\tPlease enter your first name",
                        voter_card_number: {
                            required: "\n\t\tPlease enter your National ID Number",
                            minlength: "\n\t\tminimum Length 5 character"
                        },
                        email: "\n\t\tPlease enter a valid email address",
                        contact: "\n\t\tPlease enter a Contract Number"
                    }
                }
                );
            });
        </script>
    </body>

    <%@ include file="footer.jsp"%>
</html>
