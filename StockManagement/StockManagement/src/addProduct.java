
import Database.CategoryDB;
import Database.ConnectDB;
import static Database.ConnectDB.conn;
import static Database.ConnectDB.stmt;
import Database.ProductDB;
import Model.Category;
import Model.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class addProduct extends javax.swing.JInternalFrame {

    public addProduct() {
        initComponents();
        this.setSize(640, 540);
        viewAllCategory();
        viewAllCompany();
    }

    public void viewAllCategory() {
        ProductDB aProductDB = new ProductDB();
        HashMap<String, Integer> mapCategorylist = aProductDB.CategoryView();

        //CategoryCombobox.insertItemAt("One", 0);  combobox er 0 index e value add kote hobe
        for (String s : mapCategorylist.keySet()) {
            CategoryCombobox.addItem(s);
        }
    }

    public void viewAllCompany() {
        ProductDB aProductDB = new ProductDB();
        HashMap<String, Integer> mapCompanylist = aProductDB.CompanyView();

        for (String C : mapCompanylist.keySet()) {
            companyCombobox.addItem(C);
        }
    }

    @SuppressWarnings("unchecked")

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        companyCombobox = new javax.swing.JComboBox();
        CategoryCombobox = new javax.swing.JComboBox();
        productNameTextBox = new javax.swing.JTextField();
        reorderLavelTextBox = new javax.swing.JTextField();
        SaveProductBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        setClosable(true);
        setTitle("Setup Product  Information");

        companyCombobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        companyCombobox.setForeground(new java.awt.Color(0, 102, 0));
        companyCombobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--- Select A Company ---" }));
        companyCombobox.setNextFocusableComponent(productNameTextBox);
        companyCombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                companyComboboxActionPerformed(evt);
            }
        });

        CategoryCombobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        CategoryCombobox.setForeground(new java.awt.Color(0, 102, 0));
        CategoryCombobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--- Select A Category ----" }));
        CategoryCombobox.setNextFocusableComponent(CategoryCombobox);
        CategoryCombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CategoryComboboxActionPerformed(evt);
            }
        });

        productNameTextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        productNameTextBox.setForeground(new java.awt.Color(0, 102, 0));
        productNameTextBox.setNextFocusableComponent(reorderLavelTextBox);

        reorderLavelTextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        reorderLavelTextBox.setForeground(new java.awt.Color(0, 102, 0));
        reorderLavelTextBox.setText("0");
        reorderLavelTextBox.setNextFocusableComponent(SaveProductBtn);

        SaveProductBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        SaveProductBtn.setForeground(new java.awt.Color(0, 102, 0));
        SaveProductBtn.setText("Save Product");
        SaveProductBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveProductBtnActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 0));
        jLabel3.setText("Category Name :");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 102, 0));
        jLabel4.setText("Company Name :");

        jLabel5.setFont(new java.awt.Font("Monotype Corsiva", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 102, 51));
        jLabel5.setText("    Add Product Item    ");
        jLabel5.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 0, 0)));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 0));
        jLabel6.setText("  Product Name :");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 0));
        jLabel7.setText("Reorder Level :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(492, 492, 492)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(131, 131, 131)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(SaveProductBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                                    .addGap(55, 55, 55)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(companyCombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(CategoryCombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(productNameTextBox)
                                        .addComponent(reorderLavelTextBox))))
                            .addGap(18, 18, 18)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(209, 209, 209)
                        .addComponent(jLabel5)))
                .addContainerGap(109, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CategoryCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(companyCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(productNameTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(reorderLavelTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19)
                        .addComponent(SaveProductBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(133, 133, 133))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
     int idCompany = 0;
    int idCategory = 0;
    private void CategoryComboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CategoryComboboxActionPerformed
        try {
            ProductDB aProductDB = new ProductDB();
            HashMap<String, Integer> mapCategorylist = aProductDB.CategoryView();
 //           jLabel1.setText(mapCategorylist.get(CategoryCombobox.getSelectedItem().toString()).toString());
            String CategoryId = mapCategorylist.get(CategoryCombobox.getSelectedItem().toString()).toString();
            idCategory = Integer.parseInt(CategoryId);
        } catch (Exception e) {
//
        }
    }//GEN-LAST:event_CategoryComboboxActionPerformed


    private void companyComboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_companyComboboxActionPerformed
        try {
            ProductDB aProductDB = new ProductDB();
            HashMap<String, Integer> mapCompanylist = aProductDB.CompanyView();
 //           jLabel2.setText(mapCompanylist.get(companyCombobox.getSelectedItem().toString()).toString());
            String CompanyId = mapCompanylist.get(companyCombobox.getSelectedItem().toString()).toString();
            idCompany = Integer.parseInt(CompanyId);
        } catch (Exception e) {
//
        }
    }//GEN-LAST:event_companyComboboxActionPerformed

    private void SaveProductBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveProductBtnActionPerformed

        try {
            if (idCompany > 0 && idCategory > 0) {
                ProductDB aProductDB = new ProductDB();
                Product aProduct = new Product();
                aProduct.categoryId = idCategory;
                aProduct.companyId = idCompany;
                aProduct.productName = productNameTextBox.getText();
                aProduct.reOrderLavel = Integer.parseInt(reorderLavelTextBox.getText());

                if (!productNameTextBox.getText().isEmpty()) {
                    //is exits
                    if (!aProductDB.IsExistProduct(aProduct)) {
                        String message = aProductDB.addPeoduct(aProduct);
                        JOptionPane.showMessageDialog(null, message, "Message", JOptionPane.INFORMATION_MESSAGE);
                        ProductClear();
                    } else {
                        JOptionPane.showMessageDialog(null, "Same Product Already Exists", "Error", JOptionPane.ERROR_MESSAGE);
                        ProductClear();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, " Product Name  Empty  Not Allowed", "Error", JOptionPane.ERROR_MESSAGE);
                    ProductClear();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please Select a Company And Category .", "Message", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            //
        }
    }//GEN-LAST:event_SaveProductBtnActionPerformed

    public void ProductClear() {
        // CategoryCombobox.removeAllItems();
        // companyCombobox.removeAll();
        productNameTextBox.setText("");
        reorderLavelTextBox.setText("0");
        idCompany = 0;
        idCategory = 0;
        companyCombobox.setSelectedIndex(0);
        CategoryCombobox.setSelectedIndex(0);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox CategoryCombobox;
    private javax.swing.JButton SaveProductBtn;
    private javax.swing.JComboBox companyCombobox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField productNameTextBox;
    private javax.swing.JTextField reorderLavelTextBox;
    // End of variables declaration//GEN-END:variables
}
