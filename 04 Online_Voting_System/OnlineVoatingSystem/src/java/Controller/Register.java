package Controller;

import Connection.MyDb;
import Connection.VoterRegister;
import Model.VoterRegisterM;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Register extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            PrintWriter out = response.getWriter();

            VoterRegisterM aVoterRegisterM = new VoterRegisterM();
            aVoterRegisterM.name = request.getParameter("name");
            aVoterRegisterM.surname = request.getParameter("surname");
            aVoterRegisterM.voter_card_number = request.getParameter("voter_card_number");
            aVoterRegisterM.address = request.getParameter("address");
            aVoterRegisterM.dob = request.getParameter("dob");
            aVoterRegisterM.contact = request.getParameter("contact");
            aVoterRegisterM.email = request.getParameter("email");

            VoterRegister aVoterRegister = new VoterRegister();

            if (!aVoterRegister.ISExistVoter(aVoterRegisterM)) {
                int count = aVoterRegister.VoterRegister(aVoterRegisterM);
                if (count > 0) {
                    String message = "registration success";
                    HttpSession session = request.getSession(true);
                    session.setAttribute("message", message);
                    response.sendRedirect("index.jsp?message=" + message + "");
                } else {
                    out.println("<script>alert('Register Failed.')</script>");
                }
            } else {
                String message="Already Registred";
                response.sendRedirect("reg.jsp?message=" + message + "");
            }

        } catch (Exception ex) {
            //
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
