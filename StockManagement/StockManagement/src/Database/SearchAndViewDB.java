package Database;

import static Database.ConnectDB.Query;
import static Database.ConnectDB.stmt;
import Model.Category;
import Model.Product;
import Model.SearchAndViewModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchAndViewDB extends ConnectDB {

    public List<SearchAndViewModel> SearchItemView(int Compid, int ctgid) {
        try {
            Query = "SELECT p.*,c.CompanyName,ctg.categoryName FROM tblproduct p \n"
                    + "INNER JOIN tblcompany c    ON p.companyId=c.CompanyId\n"
                    + "INNER JOIN tblcategory ctg on p.categoryId=ctg.categoryId "
                    + " where p.categoryId='" + ctgid + "' && p.companyId='" + Compid + "'";

            ResultSet rs = stmt.executeQuery(ConnectDB.Query);
            ArrayList<SearchAndViewModel> search = new ArrayList<SearchAndViewModel>();

            while (rs.next()) {
                SearchAndViewModel asearch = new SearchAndViewModel();
                asearch.productId = rs.getInt("ProductId");
                asearch.productName = rs.getString("ProductName");
                asearch.categoryId = rs.getInt("categoryId");
                asearch.categoryName = rs.getString("categoryName");
                asearch.companyId = rs.getInt("companyId");
                asearch.companyName = rs.getString("CompanyName");
                asearch.reOrderLavel = rs.getInt("ReorderLavel");
                asearch.availableQuentity = rs.getInt("AvailableQuantity");

                search.add(asearch);
            }
            return search;
        } catch (Exception e) {
            return null;
        }
    }
        
          

    public List<SearchAndViewModel> SearchItemViewForCompany(int Compid) {
        try {
            Query = "SELECT p.*,c.CompanyName,ctg.categoryName FROM tblproduct p \n"
                    + "INNER JOIN tblcompany c    ON p.companyId=c.CompanyId\n"
                    + "INNER JOIN tblcategory ctg on p.categoryId=ctg.categoryId "
                    + " where p.companyId='" + Compid + "'";

            ResultSet rs = stmt.executeQuery(ConnectDB.Query);
            ArrayList<SearchAndViewModel> search = new ArrayList<SearchAndViewModel>();

            while (rs.next()) {
                SearchAndViewModel asearch = new SearchAndViewModel();
                asearch.productId = rs.getInt("ProductId");
                asearch.productName = rs.getString("ProductName");
                asearch.categoryId = rs.getInt("categoryId");
                asearch.categoryName = rs.getString("categoryName");
                asearch.companyId = rs.getInt("companyId");
                asearch.companyName = rs.getString("CompanyName");
                asearch.reOrderLavel = rs.getInt("ReorderLavel");
                asearch.availableQuentity = rs.getInt("AvailableQuantity");

                search.add(asearch);
            }
            return search;
        } catch (Exception e) {
            return null;
        }
    }
    
    
     public List<SearchAndViewModel> SearchItemViewForcategory(int ctgid) {
        try {
            Query = "SELECT p.*,c.CompanyName,ctg.categoryName FROM tblproduct p \n"
                    + "INNER JOIN tblcompany c    ON p.companyId=c.CompanyId\n"
                    + "INNER JOIN tblcategory ctg on p.categoryId=ctg.categoryId "
                    + " where  p.categoryId='" + ctgid + "'";

            ResultSet rs = stmt.executeQuery(ConnectDB.Query);
            ArrayList<SearchAndViewModel> search = new ArrayList<SearchAndViewModel>();

            while (rs.next()) {
                SearchAndViewModel asearch = new SearchAndViewModel();
                asearch.productId = rs.getInt("ProductId");
                asearch.productName = rs.getString("ProductName");
                asearch.categoryId = rs.getInt("categoryId");
                asearch.categoryName = rs.getString("categoryName");
                asearch.companyId = rs.getInt("companyId");
                asearch.companyName = rs.getString("CompanyName");
                asearch.reOrderLavel = rs.getInt("ReorderLavel");
                asearch.availableQuentity = rs.getInt("AvailableQuantity");

                search.add(asearch);
            }
            return search;
        } catch (Exception e) {
            return null;
        }
    }
}
