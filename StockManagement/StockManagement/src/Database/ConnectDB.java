package Database;

import java.sql.*;

public class ConnectDB {

    public static Connection conn = null;
    public static Statement stmt = null;
    public static String Query;

    public ConnectDB() {
        conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/stockmanagement", "root", "");
            stmt = conn.createStatement();
        } catch (Exception e) {
            conn = null;
        }
    }

    public static Connection getConnection() {
        conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/stockmanagement", "root", "");
            stmt = conn.createStatement();
        } catch (Exception e) {
            conn = null;
        }
        return conn;
    }
}
