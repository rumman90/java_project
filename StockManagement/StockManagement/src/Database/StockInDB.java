package Database;

import static Database.ConnectDB.Query;
import static Database.ConnectDB.conn;
import static Database.ConnectDB.stmt;
import Model.Category;
import Model.Company;
import Model.Product;
import Model.StockIn;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;

public class StockInDB extends ConnectDB {

    public HashMap<String, Integer> productView(int CompanyIdrec) {
        try {
            Query = "SELECT ProductId,ProductName FROM tblproduct where companyId='" + CompanyIdrec + "'";
            ResultSet rs = stmt.executeQuery(ConnectDB.Query);

            HashMap<String, Integer> productList = new HashMap<String, Integer>();
            while (rs.next()) {
                Product aProduct = new Product();
                aProduct.productId = rs.getInt("ProductId");
                aProduct.productName = rs.getString("ProductName");
                productList.put(aProduct.productName, aProduct.productId);
            }
            return productList;
        } catch (Exception e) {
            return null;
        }
    }

    public ArrayList<StockIn> ShowProductInfo(int CompanyId, int ProductId) {
        ArrayList<StockIn> productList = new ArrayList<StockIn>();
        try {
            Query = "select ReorderLavel,AvailableQuantity from tblproduct where ProductId='" + ProductId + "' and companyId='" + CompanyId + "'";
            ResultSet rs = stmt.executeQuery(Query);
            while (rs.next()) {
                StockIn StockIn = new StockIn();
                StockIn.AvailableQuantity = rs.getInt("AvailableQuantity");
                StockIn.ReorderLevel = rs.getInt("ReorderLavel");
                productList.add(StockIn);
            }

            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return productList;
    }

    public String addStockIn(StockIn aStockin) {

        try {
            Query = "INSERT INTO tblstockin (CompanyId,ProductId,StockInQuantity) VALUES('" + aStockin.CompanyId + "','" + aStockin.ItemId + "','" + aStockin.StockInQuantity + "')";
            int count = 0;
            count = stmt.executeUpdate(Query);//insert code here
            if (count > 0) {
                return "Product Stock In Successfully";
            } else {
                return "Failed to Stock In Product Data";
            }
        } catch (Exception e) {
            return null;
        }
    }

    public boolean UpdateAvailableQ(StockIn aStockin) {
        try {
            Query = "update tblproduct set AvailableQuantity='" + aStockin.AvailableQuantity + "'where ProductId='" + aStockin.ItemId + "' and companyId='" + aStockin.CompanyId + "'";
            return stmt.executeUpdate(Query) > 0;
        } catch (Exception e) {
            return false;
        }
    }

}
