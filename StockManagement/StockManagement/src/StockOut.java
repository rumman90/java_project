
import Database.ProductDB;
import Database.StockInDB;
import Database.StockOutDb;
import Model.Company;
import Model.StockIn;
import Model.StockOutModel;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class StockOut extends javax.swing.JInternalFrame {

    public StockOut() {
        initComponents();
        this.setSize(640, 540);
        viewAllCompany();

    }

    public void viewAllCompany() {

        ProductDB aProductDB = new ProductDB();
        HashMap<String, Integer> mapCompanylist = aProductDB.CompanyView();
        for (String C : mapCompanylist.keySet()) {
            companyCombobox.addItem(C);
        }
    }

    int idCompany = 0;
    int productId = 0;
    StockInDB aStockInDB = new StockInDB();

    public void viewAllProduct() {
        itemNameCombobox.removeAllItems();
        itemNameCombobox.insertItemAt("---Select A Item-- ", 0);
        itemNameCombobox.setSelectedIndex(0);

        HashMap<String, Integer> mapProductlist;
        mapProductlist = aStockInDB.productView(idCompany);
        for (String d : mapProductlist.keySet()) {
            itemNameCombobox.addItem(d);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        stockOuttextBox = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        stockoutBtn = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        companyCombobox = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        itemNameCombobox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        reorderLevelTextBox = new javax.swing.JTextField();
        availableQuentitytextBox = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        StockOutTable = new javax.swing.JTable();
        saleBtn = new javax.swing.JButton();
        DamageBtn = new javax.swing.JButton();
        lostBtn = new javax.swing.JButton();

        setClosable(true);
        setTitle("Stock Out");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 0));
        jLabel6.setText("Reorder Level :");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 0));
        jLabel7.setText("Available Quentity :");

        stockOuttextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockOuttextBox.setForeground(new java.awt.Color(0, 102, 0));

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 0));
        jLabel8.setText("Product Stock Out :");

        stockoutBtn.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        stockoutBtn.setForeground(new java.awt.Color(0, 102, 0));
        stockoutBtn.setText("Stock Out Item");
        stockoutBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stockoutBtnActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 102, 0));
        jLabel4.setText("Company Name :");

        companyCombobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        companyCombobox.setForeground(new java.awt.Color(0, 102, 0));
        companyCombobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--- Select A Company ---" }));
        companyCombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                companyComboboxActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Monotype Corsiva", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 102, 51));
        jLabel5.setText("  Product Stock Out  ");
        jLabel5.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 0, 0)));

        itemNameCombobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameCombobox.setForeground(new java.awt.Color(0, 102, 0));
        itemNameCombobox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "---Select A Item---" }));
        itemNameCombobox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                itemNameComboboxItemStateChanged(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 0));
        jLabel1.setText(" Item Name :");

        reorderLevelTextBox.setEditable(false);
        reorderLevelTextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        reorderLevelTextBox.setForeground(new java.awt.Color(0, 102, 0));

        availableQuentitytextBox.setEditable(false);
        availableQuentitytextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        availableQuentitytextBox.setForeground(new java.awt.Color(0, 102, 0));

        StockOutTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Serial", "Item", "Company", "StockOut Quen:"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        StockOutTable.setColumnSelectionAllowed(true);
        jScrollPane1.setViewportView(StockOutTable);
        StockOutTable.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (StockOutTable.getColumnModel().getColumnCount() > 0) {
            StockOutTable.getColumnModel().getColumn(0).setResizable(false);
            StockOutTable.getColumnModel().getColumn(1).setResizable(false);
            StockOutTable.getColumnModel().getColumn(2).setResizable(false);
            StockOutTable.getColumnModel().getColumn(3).setResizable(false);
        }

        saleBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        saleBtn.setForeground(new java.awt.Color(0, 102, 0));
        saleBtn.setText("Sale");
        saleBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saleBtnActionPerformed(evt);
            }
        });

        DamageBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        DamageBtn.setForeground(new java.awt.Color(0, 102, 0));
        DamageBtn.setText("Damage");
        DamageBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DamageBtnActionPerformed(evt);
            }
        });

        lostBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lostBtn.setForeground(new java.awt.Color(0, 102, 0));
        lostBtn.setText("Lost");
        lostBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lostBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(DamageBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(saleBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lostBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 16, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(94, 94, 94)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel4))
                                .addGap(45, 45, 45)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(itemNameCombobox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(companyCombobox, javax.swing.GroupLayout.Alignment.LEADING, 0, 195, Short.MAX_VALUE)
                                    .addComponent(reorderLevelTextBox)
                                    .addComponent(availableQuentitytextBox)
                                    .addComponent(stockOuttextBox, javax.swing.GroupLayout.Alignment.LEADING)))
                            .addComponent(stockoutBtn))
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(80, 80, 80))
            .addGroup(layout.createSequentialGroup()
                .addGap(227, 227, 227)
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(companyCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(itemNameCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))))
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(reorderLevelTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(availableQuentitytextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stockOuttextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(stockoutBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(saleBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(DamageBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lostBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(346, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    ArrayList<StockOutModel> stckOutInfo = new ArrayList<StockOutModel>();
    private void stockoutBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stockoutBtnActionPerformed
        try {
            if (idCompany > 0 && productId > 0) {
                if (!stockOuttextBox.getText().isEmpty() && Integer.parseInt(stockOuttextBox.getText()) > 0) {
                    int AvailableQuantity = Integer.parseInt(availableQuentitytextBox.getText());
                    int StockOutQuantity = Integer.parseInt(stockOuttextBox.getText());
                    int ReorderLevel = Integer.parseInt(reorderLevelTextBox.getText());
                    if (AvailableQuantity > (StockOutQuantity + ReorderLevel)) {

                        StockOutModel aStockOut = new StockOutModel();
                        aStockOut.CompanyId = idCompany;
                        aStockOut.ItemId = productId;
                        aStockOut.ReorderLevel = ReorderLevel;
                        aStockOut.AvailableQuantity = AvailableQuantity;
                        aStockOut.StockOutQuantity = StockOutQuantity;

                        //item already exit check
                        int a = 10;
                        for (StockOutModel name : stckOutInfo) {
                            if (name.CompanyId == aStockOut.CompanyId && name.ItemId == aStockOut.ItemId) {
                                a = 0;
                            } else {
                                a = 1;
                            }
                        }

                        if (a > 0) {
                            stckOutInfo.add(aStockOut);// data array list e add

                        } else {
                            JOptionPane.showMessageDialog(null, " Item already Exit  ", "Message", JOptionPane.WARNING_MESSAGE);
                        }
                        //edit check
                        showtabledata();
                        clearStockOutData();
                    } else {
                        JOptionPane.showMessageDialog(null, " Product not available in our stock. ", "Message", JOptionPane.WARNING_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "StockOut value  Null Or less then 0 Not Allowed", "Message", JOptionPane.WARNING_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please Select a Company And Item .", "Message", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            //
        }
    }//GEN-LAST:event_stockoutBtnActionPerformed

    public void clearStockOutData() {

        idCompany = 0;
        productId = 0;
        availableQuentitytextBox.setText("");
        reorderLevelTextBox.setText("");
        stockOuttextBox.setText("");
        companyCombobox.setSelectedIndex(0);
        itemNameCombobox.setSelectedIndex(0);

    }

    DefaultTableModel model = null;

    public void showtabledata() {
        // table data show
        model = (DefaultTableModel) StockOutTable.getModel();
        model.setRowCount(0);    //for clear Table Data
        int count = 0;
        for (StockOutModel astckOutInfo : stckOutInfo) {
            StockOutModel aaStockOut = new StockOutModel();
            aaStockOut.CompanyId = astckOutInfo.CompanyId;
            aaStockOut.ItemId = astckOutInfo.ItemId;
            aaStockOut.ReorderLevel = astckOutInfo.ReorderLevel;
            aaStockOut.AvailableQuantity = astckOutInfo.AvailableQuantity;
            aaStockOut.StockOutQuantity = astckOutInfo.StockOutQuantity;
            int c = ++count;
            model.addRow(new Object[]{c, aaStockOut.CompanyId, aaStockOut.ItemId, aaStockOut.StockOutQuantity});
        }
    }

    private void companyComboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_companyComboboxActionPerformed
        try {
            ProductDB aProductDB = new ProductDB();
            HashMap<String, Integer> mapCompanylist = aProductDB.CompanyView();
           // jLabel2.setText(mapCompanylist.get(companyCombobox.getSelectedItem().toString()).toString());
            String CompanyId = mapCompanylist.get(companyCombobox.getSelectedItem().toString()).toString();
            idCompany = Integer.parseInt(CompanyId);

            viewAllProduct();
        } catch (Exception e) {
//
        }
    }//GEN-LAST:event_companyComboboxActionPerformed

    private void itemNameComboboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_itemNameComboboxItemStateChanged
        try {
            HashMap<String, Integer> mapProductlist = aStockInDB.productView(idCompany);
            if (evt.getStateChange() == ItemEvent.SELECTED) {
                if (this.itemNameCombobox.getSelectedIndex() > 0) {
                 //   jLabel3.setText(mapProductlist.get(itemNameCombobox.getSelectedItem().toString()).toString());
                    String ProductId = mapProductlist.get(itemNameCombobox.getSelectedItem().toString()).toString();
                    productId = Integer.parseInt(ProductId);

                    ArrayList<StockIn> ProductResult = aStockInDB.ShowProductInfo(idCompany, productId);
                    for (StockIn result1 : ProductResult) {
                        String AvailableQuantity = String.valueOf(result1.AvailableQuantity);
                        String reorderLevelText = String.valueOf(result1.ReorderLevel);
                        reorderLevelTextBox.setText(reorderLevelText);
                        availableQuentitytextBox.setText(AvailableQuantity);
                    }
                }
            }
        } catch (Exception e) {
//
        }
    }//GEN-LAST:event_itemNameComboboxItemStateChanged

    private void saleBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saleBtnActionPerformed
        int outType = 1;
        AddStockOut(outType);
        model.setRowCount(0);
        stckOutInfo.clear();
    }//GEN-LAST:event_saleBtnActionPerformed

    private void DamageBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DamageBtnActionPerformed
        int outType = 2;
        AddStockOut(outType);
        model.setRowCount(0);
        stckOutInfo.clear();
    }//GEN-LAST:event_DamageBtnActionPerformed

    private void lostBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lostBtnActionPerformed
        int outType = 3;
        AddStockOut(outType);
        model.setRowCount(0);
        stckOutInfo.clear();
    }//GEN-LAST:event_lostBtnActionPerformed

    private void AddStockOut(int type) {
        StockOutDb aStockOutDb = new StockOutDb();
        String message = "Data Not Found";
        for (StockOutModel info : stckOutInfo) {
            StockOutModel stockOut = new StockOutModel();
            stockOut.CompanyId = info.CompanyId;
            stockOut.ItemId = info.ItemId;
            stockOut.ReorderLevel = info.ReorderLevel;
            stockOut.AvailableQuantity = info.AvailableQuantity;
            stockOut.StockOutQuantity = info.StockOutQuantity;
            stockOut.OutType = type;
            stockOut.AvailableQuantity = stockOut.AvailableQuantity - stockOut.StockOutQuantity;

            boolean result = aStockOutDb.UpdateAvailableQ(stockOut);
            if (result == true) {
                message = aStockOutDb.InsertStockOut(stockOut);
            }
        }
        JOptionPane.showMessageDialog(null, message);

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton DamageBtn;
    private javax.swing.JTable StockOutTable;
    private javax.swing.JTextField availableQuentitytextBox;
    private javax.swing.JComboBox companyCombobox;
    private javax.swing.JComboBox<String> itemNameCombobox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton lostBtn;
    private javax.swing.JTextField reorderLevelTextBox;
    private javax.swing.JButton saleBtn;
    private javax.swing.JTextField stockOuttextBox;
    private javax.swing.JButton stockoutBtn;
    // End of variables declaration//GEN-END:variables
}
