package Database;

import Model.Category;
import Model.Company;
import Model.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JComboBox;

public class ProductDB extends ConnectDB {

    public HashMap<String, Integer> CategoryView() {
        try {
            Query = "SELECT categoryId,categoryName FROM tblcategory";
            ResultSet rs = stmt.executeQuery(ConnectDB.Query);

            HashMap<String, Integer> CategoryList = new HashMap<String, Integer>();
            while (rs.next()) {
                Category aCategory = new Category();
                aCategory.Categoryid = rs.getInt(1);
                aCategory.CategoryName = rs.getString(2);
                CategoryList.put(aCategory.CategoryName, aCategory.Categoryid);
            }
            return CategoryList;
        } catch (Exception e) {
            return null;
        }
    }

    public HashMap<String, Integer> CompanyView() {
        try {
            Query = "SELECT CompanyId,CompanyName FROM tblcompany";
            ResultSet rs = stmt.executeQuery(ConnectDB.Query);

            HashMap<String, Integer> CompanyList = new HashMap<String, Integer>();
            while (rs.next()) {
                Company aCompany = new Company();
                aCompany.CompanyID = rs.getInt(1);
                aCompany.CompanyName = rs.getString(2);
                CompanyList.put(aCompany.CompanyName, aCompany.CompanyID);
            }
            return CompanyList;
        } catch (Exception e) {
            return null;
        }
    }

    public String addPeoduct(Product aProduct) {

        try {
            Query = "INSERT INTO tblProduct (categoryId,companyId,ProductName,ReorderLavel) VALUES('" + aProduct.categoryId + "','" + aProduct.companyId + "','" + aProduct.productName + "','" + aProduct.reOrderLavel + "')";
            int count = 0;
            count = stmt.executeUpdate(Query);//insert code here
            if (count > 0) {
                return "Product Item  Insert Successfully";
            } else {
                return "Failed to save Product Item";
            }
        } catch (Exception e) {
            return null;
        }
    }

    public boolean IsExistProduct(Product aProduct) {
        try {
            Query = "SELECT * FROM tblProduct WHERE companyId='" + aProduct.companyId + "' and ProductName='" + aProduct.productName + "'";
            ResultSet rs = stmt.executeQuery(Query);
            if (rs.next() == true) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}
