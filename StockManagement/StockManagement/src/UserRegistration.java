
import Database.UserRegistrationDB;
import Model.UserRegistrationModel;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class UserRegistration extends javax.swing.JInternalFrame {

    public UserRegistration() {
        initComponents();
        this.setSize(640, 540);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        browseBtn = new javax.swing.JButton();
        imageNameLabel = new javax.swing.JLabel();
        labelImageShow = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        userNameTextBox = new javax.swing.JTextField();
        userEmailTextBox = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        createBtn = new javax.swing.JButton();
        passwordTextBox = new javax.swing.JPasswordField();
        confarmPassTextBox = new javax.swing.JPasswordField();

        setClosable(true);
        setTitle("Create A New User   ");

        browseBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        browseBtn.setText("Browse...");
        browseBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseBtnActionPerformed(evt);
            }
        });

        imageNameLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        labelImageShow.setBorder(javax.swing.BorderFactory.createCompoundBorder(null, javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 0, 0)), new javax.swing.border.LineBorder(new java.awt.Color(0, 102, 0), 2, true))));

        jLabel1.setFont(new java.awt.Font("Monotype Corsiva", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("  Create A New User   ");
        jLabel1.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 0, 0)));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("User Name :");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Email :");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Password :");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Confarm Password :");

        userNameTextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        userEmailTextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Image Name :");

        createBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        createBtn.setForeground(new java.awt.Color(255, 0, 0));
        createBtn.setText("Create");
        createBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createBtnActionPerformed(evt);
            }
        });

        passwordTextBox.setText("jPasswordField1");
        passwordTextBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                passwordTextBoxMouseClicked(evt);
            }
        });

        confarmPassTextBox.setText("jPasswordField1");
        confarmPassTextBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                confarmPassTextBoxMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(187, 187, 187)
                .addComponent(jLabel1))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(105, 105, 105)
                        .addComponent(jLabel3))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(jLabel4))
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(userNameTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(userEmailTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(passwordTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(confarmPassTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addComponent(labelImageShow, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(imageNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(browseBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addGap(252, 252, 252)
                .addComponent(createBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel1)
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelImageShow, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(jLabel2)
                                .addGap(27, 27, 27)
                                .addComponent(jLabel3)
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(passwordTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(userNameTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(13, 13, 13)
                                .addComponent(userEmailTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(confarmPassTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel6))
                    .addComponent(imageNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(browseBtn))
                .addGap(18, 18, 18)
                .addComponent(createBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    String fileName;
    File selectedFile;
    String sourcePath;
    private void browseBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseBtnActionPerformed

        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.IMAGE", "jpg", "gif", "png");
        fileChooser.addChoosableFileFilter(filter);
        int result = fileChooser.showSaveDialog(null);

        if (result == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile();
            sourcePath = selectedFile.getAbsolutePath();  //file path  Select include name
            fileName = selectedFile.getName();            //file name Select   

            imageNameLabel.setText(fileName);
            imageIconforLabel(sourcePath);
        } else if (result == JFileChooser.CANCEL_OPTION) {
            System.out.println("No Image Found");
        }
    }//GEN-LAST:event_browseBtnActionPerformed
    UserRegistrationDB aUserRegiDB = new UserRegistrationDB();
    private void createBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createBtnActionPerformed
        String myPass1 = String.valueOf(passwordTextBox.getPassword());
        String myPass2 = String.valueOf(confarmPassTextBox.getPassword());

        if (!userNameTextBox.getText().isEmpty() && !userEmailTextBox.getText().isEmpty() && !myPass2.isEmpty()
                && !myPass1.isEmpty() && !imageNameLabel.getText().isEmpty()) {

            if (myPass1.equals(myPass2)) {
                String projectDirectory = System.getProperty("user.dir");                       //projec directory
                File projectDirectoryFolder = new File(projectDirectory + "\\src\\images\\" + fileName);
                Path movefrom = FileSystems.getDefault().getPath(selectedFile.getPath());       //selected file image source location

                UserRegistrationModel aUserRegiModel = new UserRegistrationModel();
                aUserRegiModel.userName = userNameTextBox.getText();
                aUserRegiModel.email = userEmailTextBox.getText();
                aUserRegiModel.password = passwordTextBox.getText();
                aUserRegiModel.image = fileName;
                try {
                    boolean message = aUserRegiDB.createUser(aUserRegiModel);
                    if (message == true) {
                        Files.copy(movefrom, projectDirectoryFolder.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        JOptionPane.showMessageDialog(null, "User Create SuccessFully", "Error Message", JOptionPane.INFORMATION_MESSAGE);
                        clear();
                    } else {
                        JOptionPane.showMessageDialog(null, "Failed to create User", "Error Message", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (IOException e) {
                    //
                }

            } else {
                JOptionPane.showMessageDialog(null, "Password is not Matched", "Error Message", JOptionPane.ERROR_MESSAGE);

            }
        } else {
            JOptionPane.showMessageDialog(null, "All Field Are Required !!", "Error Message", JOptionPane.ERROR_MESSAGE); 
        }
    }//GEN-LAST:event_createBtnActionPerformed

    private void clear() {
        userNameTextBox.setText("");
        userEmailTextBox.setText("");
        passwordTextBox.setText("123456");
        confarmPassTextBox.setText("123456");
        imageNameLabel.setText("");
        labelImageShow.setIcon(null);
        fileName = "";
        selectedFile = null;
        sourcePath = "";
    }


    private void passwordTextBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_passwordTextBoxMouseClicked
        passwordTextBox.setText("");
    }//GEN-LAST:event_passwordTextBoxMouseClicked

    private void confarmPassTextBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_confarmPassTextBoxMouseClicked
        confarmPassTextBox.setText("");
    }//GEN-LAST:event_confarmPassTextBoxMouseClicked

    private void imageIconforLabel(String sourcePath) {
        ImageIcon imageIcon = new ImageIcon(sourcePath);
        Image myImage = imageIcon.getImage();
        Image newImage = myImage.getScaledInstance(labelImageShow.getWidth(), labelImageShow.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImage);
        labelImageShow.setIcon(image);

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton browseBtn;
    private javax.swing.JPasswordField confarmPassTextBox;
    private javax.swing.JButton createBtn;
    private javax.swing.JLabel imageNameLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel labelImageShow;
    private javax.swing.JPasswordField passwordTextBox;
    private javax.swing.JTextField userEmailTextBox;
    private javax.swing.JTextField userNameTextBox;
    // End of variables declaration//GEN-END:variables
}
