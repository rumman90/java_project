
import Connection.MyDb;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class AdminLogin extends HttpServlet {

 
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
            String admin_name = request.getParameter("admin_name");
            String admin_pass = request.getParameter("admin_pass");
            
            
            
          MyDb db = new MyDb();
          Connection con = db.getCon();
            try {
                Statement stmt = con.createStatement();
               ResultSet rs =  stmt.executeQuery("select * from admin_login where user_name = '"+admin_name+"' and password = '"+admin_pass+"'");
               if(rs.next()){
                   HttpSession  session = request.getSession();
                   session.setAttribute("adminname", admin_name);
                   response.sendRedirect("adminwelcome.jsp");
               }else{
                      out.println("Wrong Password or user name");
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(AdminLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
          
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
