package Database;

import static Database.ConnectDB.Query;
import static Database.ConnectDB.stmt;
import Model.UserRegistrationModel;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class UserInfoDB {

    public ArrayList<UserRegistrationModel> validUserInfo(int userId) {
        try {
            Query = "SELECT * FROM user where id='" + userId + "'";
            ResultSet rs = stmt.executeQuery(Query);
            ArrayList<UserRegistrationModel> UserList = new ArrayList<UserRegistrationModel>();
            while (rs.next()) {
                UserRegistrationModel user = new UserRegistrationModel();
                user.id = rs.getInt(1);
                user.userName = rs.getString(2);
                user.email = rs.getString(4);
                user.image = rs.getString(5);
                UserList.add(user);
            }
            return UserList;
        } catch (Exception e) {
            return null;
        }

    }
}
