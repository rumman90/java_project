package Model;

public class StockOutModel {

    public int CompanyId;
    public String CompanyName;
    public int ItemId;
    public String ItemName;
    public int ReorderLevel;
    public int AvailableQuantity;
    public int StockOutQuantity;
    public int OutType;
}
