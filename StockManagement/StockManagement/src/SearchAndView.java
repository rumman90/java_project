
import Database.ProductDB;
import Database.SearchAndViewDB;
import Model.SearchAndViewModel;
import Model.StockOutModel;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import java.text.*;
import java.awt.print.*;
import java.util.Date;
import javax.swing.JTable;

public class SearchAndView extends javax.swing.JInternalFrame {

    public SearchAndView() {
        initComponents();
        this.setSize(640, 540);
        viewAllCompany();
        viewAllCategory();

        searchTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        searchTable.getColumnModel().getColumn(0).setPreferredWidth(40); //serial column width
        searchTable.getColumnModel().getColumn(1).setPreferredWidth(100);
        searchTable.getColumnModel().getColumn(2).setPreferredWidth(100);
        searchTable.getColumnModel().getColumn(3).setPreferredWidth(100);
        searchTable.getColumnModel().getColumn(4).setPreferredWidth(70);
        searchTable.getColumnModel().getColumn(5).setPreferredWidth(70);

    }

    public void viewAllCompany() {
        ProductDB aProductDB = new ProductDB();
        // companyCombobox.insertItemAt("---Select A Company-- ", 0);
        //companyCombobox.setSelectedIndex(0);
        HashMap<String, Integer> mapCompanylist = aProductDB.CompanyView();
        for (String C : mapCompanylist.keySet()) {
            companyCombobox.addItem(C);
        }
    }

    public void viewAllCategory() {
        ProductDB aProductDB = new ProductDB();
        HashMap<String, Integer> mapCategorylist = aProductDB.CategoryView();
        for (String category : mapCategorylist.keySet()) {
            CategoryCombobox.addItem(category);
        }
    }

    @SuppressWarnings("unchecked")

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        companyCombobox = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        CategoryCombobox = new javax.swing.JComboBox<>();
        sa = new javax.swing.JLabel();
        sa2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        searchTable = new javax.swing.JTable();
        SearchBtn = new javax.swing.JButton();
        printBtn = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        ClearBtn = new javax.swing.JButton();

        setClosable(true);
        setTitle("Search & View Items Summary ");
        getContentPane().setLayout(null);

        companyCombobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        companyCombobox.setForeground(new java.awt.Color(0, 102, 0));
        companyCombobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--- Select A Company ---" }));
        companyCombobox.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        companyCombobox.setNextFocusableComponent(CategoryCombobox);
        companyCombobox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                companyComboboxItemStateChanged(evt);
            }
        });
        getContentPane().add(companyCombobox);
        companyCombobox.setBounds(240, 110, 178, 31);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 102, 0));
        jLabel4.setText("Company :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(130, 110, 69, 27);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 0));
        jLabel1.setText("Category :");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(130, 160, 66, 17);

        CategoryCombobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        CategoryCombobox.setForeground(new java.awt.Color(0, 102, 0));
        CategoryCombobox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "---Select A Category---" }));
        CategoryCombobox.setNextFocusableComponent(SearchBtn);
        CategoryCombobox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                CategoryComboboxItemStateChanged(evt);
            }
        });
        getContentPane().add(CategoryCombobox);
        CategoryCombobox.setBounds(240, 160, 178, 31);
        getContentPane().add(sa);
        sa.setBounds(80, 110, 43, 20);
        getContentPane().add(sa2);
        sa2.setBounds(80, 160, 43, 9);

        searchTable.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        searchTable.setForeground(new java.awt.Color(0, 102, 0));
        searchTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SL", "Item Name", "Comapany", "Category", "Av. Quantity", "Reorder L."
            }
        ));
        jScrollPane2.setViewportView(searchTable);

        getContentPane().add(jScrollPane2);
        jScrollPane2.setBounds(60, 270, 490, 200);

        SearchBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        SearchBtn.setForeground(new java.awt.Color(0, 102, 0));
        SearchBtn.setText("Search");
        SearchBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SearchBtnActionPerformed(evt);
            }
        });
        getContentPane().add(SearchBtn);
        SearchBtn.setBounds(430, 210, 120, 31);

        printBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        printBtn.setForeground(new java.awt.Color(0, 102, 0));
        printBtn.setText("Print");
        printBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                printBtnActionPerformed(evt);
            }
        });
        getContentPane().add(printBtn);
        printBtn.setBounds(330, 210, 94, 31);

        jLabel2.setFont(new java.awt.Font("Monotype Corsiva", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 102, 51));
        jLabel2.setText("  Search & View Items Summary   ");
        jLabel2.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 0, 0)));
        getContentPane().add(jLabel2);
        jLabel2.setBounds(148, 37, 328, 32);

        ClearBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        ClearBtn.setForeground(new java.awt.Color(255, 0, 0));
        ClearBtn.setText("Clear");
        ClearBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClearBtnActionPerformed(evt);
            }
        });
        getContentPane().add(ClearBtn);
        ClearBtn.setBounds(240, 210, 79, 31);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    int Companyid = 0;
    int categoryid = 0;
    List<SearchAndViewModel> aList;
    private void SearchBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SearchBtnActionPerformed
        try {
            SearchAndViewDB asearchDB = new SearchAndViewDB();
            if (Companyid > 0 && categoryid > 0) {
                aList = asearchDB.SearchItemView(Companyid, categoryid);
                showSearchdata();
                clearData();
            } else if (Companyid > 0) {
                aList = asearchDB.SearchItemViewForCompany(Companyid);
                showSearchdata();
                clearData();
            } else if (categoryid > 0) {
                aList = asearchDB.SearchItemViewForcategory(categoryid);
                showSearchdata();
                clearData();
            } else {
                JOptionPane.showMessageDialog(null, "Please Select a Company Or Category  Or Both.", "Message", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            //    
        }
    }//GEN-LAST:event_SearchBtnActionPerformed

    private void companyComboboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_companyComboboxItemStateChanged
        try {
            ProductDB aProductDB = new ProductDB();
            HashMap<String, Integer> mapCompanylist = aProductDB.CompanyView();
            // sa.setText(mapCompanylist.get(companyCombobox.getSelectedItem().toString()).toString());
            String CompanyId = mapCompanylist.get(companyCombobox.getSelectedItem().toString()).toString();
            Companyid = Integer.parseInt(CompanyId);
        } catch (Exception a) {
            //
        }
    }//GEN-LAST:event_companyComboboxItemStateChanged

    private void CategoryComboboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_CategoryComboboxItemStateChanged
        try {
            ProductDB aProductDB = new ProductDB();
            HashMap<String, Integer> mapCategorylist = aProductDB.CategoryView();
            //sa2.setText(mapCategorylist.get(CategoryCombobox.getSelectedItem().toString()).toString());
            String CompanyId = mapCategorylist.get(CategoryCombobox.getSelectedItem().toString()).toString();
            categoryid = Integer.parseInt(CompanyId);
        } catch (Exception a) {
            //
        }
    }//GEN-LAST:event_CategoryComboboxItemStateChanged

    private void printBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printBtnActionPerformed

        MessageFormat header = new MessageFormat("Report Print");
        MessageFormat footer = new MessageFormat("page{0,number,integer}");
        try {
            searchTable.print(JTable.PrintMode.FIT_WIDTH, header, footer);

        } catch (java.awt.print.PrinterException e) {
            System.err.println("can not print");
        }
    }//GEN-LAST:event_printBtnActionPerformed

    private void ClearBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClearBtnActionPerformed
        try {
            aList.clear();
            Companyid = 0;
            categoryid = 0;
            model.setRowCount(0);
            CategoryCombobox.setSelectedIndex(0);
            companyCombobox.setSelectedIndex(0);

        } catch (Exception e) {
            //
        }
    }//GEN-LAST:event_ClearBtnActionPerformed

    private void clearData() {
        aList.clear();
        Companyid = 0;
        categoryid = 0;
        CategoryCombobox.setSelectedIndex(0);
        companyCombobox.setSelectedIndex(0);
    }
    DefaultTableModel model;

    public void showSearchdata() {
        // table data show 
        model = (DefaultTableModel) searchTable.getModel();
        model.setRowCount(0);    //for clear Table Data
        int count = 0;
        for (SearchAndViewModel result : aList) {
            SearchAndViewModel aSearchResult = new SearchAndViewModel();
            aSearchResult.productName = result.productName;
            aSearchResult.companyName = result.companyName;
            aSearchResult.categoryName = result.categoryName;
            aSearchResult.availableQuentity = result.availableQuentity;
            aSearchResult.reOrderLavel = result.reOrderLavel;
            int c = ++count;
            model.addRow(new Object[]{c, aSearchResult.productName, aSearchResult.companyName, aSearchResult.categoryName, aSearchResult.availableQuentity, aSearchResult.reOrderLavel});
        }

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> CategoryCombobox;
    private javax.swing.JButton ClearBtn;
    private javax.swing.JButton SearchBtn;
    private javax.swing.JComboBox companyCombobox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton printBtn;
    private javax.swing.JLabel sa;
    private javax.swing.JLabel sa2;
    private javax.swing.JTable searchTable;
    // End of variables declaration//GEN-END:variables
}
