

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/home.css"/>
        <%@ include file="header.jsp"%>
        <title>JSP Page</title>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            input[type=text], select, textarea {
                width: 100%;
                padding: 12px;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
                margin-top: 6px;
                margin-bottom: 16px;
                resize: vertical;
            }

            input[type=submit] {
                background-color: #4CAF50;
                color: white;
                padding: 12px 20px;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            .container {
                border-radius: 5px;
                background-color: #f2f2f2;
                padding: 20px;
            }
        </style>
    </head>
    <body>



        <div class="container" style="width: 50%;margin-left:25%;margin-bottom: 40px">
            <h3>Contact Us</h3>
            <p style="margin-left: 60%;">
                <b style="font-size: 20px;"> Mr.Rumman Islam Nur</b><br>
                <b>Address:</b> :Dhaka Bangladesh<br>
                <b>Phone</b> : +01744821090<br>
                <b>E-Mail</b> : rumman821090@gmail.com <hr>OR

            </p>
            <form action="">
                <label for="fname">First Name</label>
                <input type="text" id="fname" name="firstname" placeholder="Your name..">

                <label for="lname">Subject</label>
                <input type="text" id="lname" name="lastname" placeholder="Subject..">
                <label for="subject">Message</label>
                <textarea id="subject" name="subject" placeholder="Write something.." style="height:200px"></textarea>

                <input type="submit" value="Submit">
            </form>
        </div>

        <div style="margin-top: 30px;">
            <%@ include file="footer.jsp"%>
        </div>
    </body>
</html>
