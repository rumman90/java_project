package Database;

import Model.Company;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;

public class CompanyDB extends ConnectDB {

    public String SaveCompany(Company aCompany) {
        try {
            Query = "INSERT INTO tblcompany(CompanyName,CompanyDescription) VALUES('" + aCompany.CompanyName + "','" + aCompany.CompanyDescription + "')";
            int count = 0;
            count = stmt.executeUpdate(Query);
            if (count > 0) {
                return "Company Name Insert Successfully";
            } else {
                return "Failed to save Company Data";
            }
        } catch (Exception e) {
            return null;
        }
    }

   public List<Company> ViewAllCompany() {
        try {
            Query = "SELECT * FROM tblcompany";
            ResultSet rs =stmt.executeQuery(Query);
            List<Company> companyList = new ArrayList<Company>();
            while (rs.next()) {
                Company company = new Company();
                company.CompanyID = rs.getInt(1);
                company.CompanyName = rs.getString(2);
                company.CompanyDescription = rs.getString(3);
                companyList.add(company);
            }
            return companyList;
        } catch (Exception e) {
            return null;
        }

   }
    

    public boolean IsExistCompany(Company aCompany) {
        try {
            Query = "SELECT * FROM tblcompany where CompanyName='" + aCompany.CompanyName + "'";
            ResultSet rs = stmt.executeQuery(Query);
            if (rs.next() == true) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean UpdateCompany(Company aCompany) {
        try {
            Query = "update tblcompany set CompanyName='" + aCompany.CompanyName + "' ,CompanyDescription= '" + aCompany.CompanyDescription + "' where CompanyId='" + aCompany.CompanyID + "'";   
            return stmt.executeUpdate(Query) > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean DeleteCompany(Company aCompany) {
        try {
            Query = "delete from tblcompany where CompanyId='" + aCompany.CompanyID + "' ";
            return ConnectDB.stmt.executeUpdate(Query) > 0;
        } catch (Exception e) {
            return false;
        }
    }

}
