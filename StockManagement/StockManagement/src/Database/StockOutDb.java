package Database;

import static Database.ConnectDB.Query;
import static Database.ConnectDB.stmt;
import Model.StockIn;
import Model.StockOutModel;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Nur (R)
 */
public class StockOutDb extends ConnectDB {
  Date date = new Date();
 SimpleDateFormat ft = new SimpleDateFormat ("yyyy.MM.dd");
 String  adate= ft.format(date);
 
    public String InsertStockOut(StockOutModel aModel) {
        try {
            Query = "INSERT INTO tblStockOut (Itemid,companyId,StockOutQuantity,Status,date) VALUES('" + aModel.ItemId + "','" + aModel.CompanyId + "','" + aModel.StockOutQuantity + "','" + aModel.OutType + "','" +adate + "')";
            int count = 0;
            count = stmt.executeUpdate(Query);//insert code here
            if (count > 0) { 
                return "Stock Out Successfully";
            } else {
                return "Failed to Stock Out ";
            }
        } catch (Exception e) {
            return null;
        }
    }

    public boolean UpdateAvailableQ(StockOutModel aModel) {
        try {
            Query = "update tblproduct set AvailableQuantity='" + aModel.AvailableQuantity + "'where ProductId='" + aModel.ItemId + "' and companyId='" + aModel.CompanyId + "'";
            return stmt.executeUpdate(Query) > 0;
        } catch (Exception e) {
            return false;
        }
    }
}
