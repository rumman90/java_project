-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2018 at 03:38 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stockmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblcategory`
--

CREATE TABLE `tblcategory` (
  `categoryId` int(10) NOT NULL,
  `categoryName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblcategory`
--

INSERT INTO `tblcategory` (`categoryId`, `categoryName`) VALUES
(20, 'Mobile'),
(21, 'Laptop');

-- --------------------------------------------------------

--
-- Table structure for table `tblcompany`
--

CREATE TABLE `tblcompany` (
  `CompanyId` int(10) NOT NULL,
  `CompanyName` varchar(50) NOT NULL,
  `CompanyDescription` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblcompany`
--

INSERT INTO `tblcompany` (`CompanyId`, `CompanyName`, `CompanyDescription`) VALUES
(10, 'Dell', 'delll'),
(11, 'asus', 'asus');

-- --------------------------------------------------------

--
-- Table structure for table `tblproduct`
--

CREATE TABLE `tblproduct` (
  `ProductId` int(20) NOT NULL,
  `categoryId` int(20) NOT NULL,
  `companyId` int(20) NOT NULL,
  `ProductName` varchar(50) NOT NULL,
  `ReorderLavel` int(20) NOT NULL,
  `AvailableQuantity` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblproduct`
--

INSERT INTO `tblproduct` (`ProductId`, `categoryId`, `companyId`, `ProductName`, `ReorderLavel`, `AvailableQuantity`) VALUES
(15, 21, 10, 'dell 10', 5, 50),
(16, 21, 10, 'dell 20', 8, 40),
(18, 20, 11, 'asus 31', 6, 80);

-- --------------------------------------------------------

--
-- Table structure for table `tblstockin`
--

CREATE TABLE `tblstockin` (
  `StockInId` int(11) NOT NULL,
  `CompanyId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `StockInQuantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblstockin`
--

INSERT INTO `tblstockin` (`StockInId`, `CompanyId`, `ProductId`, `StockInQuantity`) VALUES
(1, 10, 15, 50),
(2, 10, 16, 56);

-- --------------------------------------------------------

--
-- Table structure for table `tblstockout`
--

CREATE TABLE `tblstockout` (
  `StockOutId` int(11) NOT NULL,
  `Itemid` int(11) NOT NULL,
  `companyId` int(11) NOT NULL,
  `StockOutQuantity` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblstockout`
--

INSERT INTO `tblstockout` (`StockOutId`, `Itemid`, `companyId`, `StockOutQuantity`, `Status`, `date`) VALUES
(2, 18, 11, 9, 1, '2018-03-14'),
(3, 16, 10, 2, 1, '2018-03-14');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Images` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `Name`, `password`, `Email`, `Images`) VALUES
(1, 'admin', '123456', 'admin@gmail.com', 'admin.png'),
(2, 'Rumman', '123456', 'rumman', 'rumman.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblcategory`
--
ALTER TABLE `tblcategory`
  ADD PRIMARY KEY (`categoryId`);

--
-- Indexes for table `tblcompany`
--
ALTER TABLE `tblcompany`
  ADD PRIMARY KEY (`CompanyId`),
  ADD UNIQUE KEY `CompanyName` (`CompanyName`);

--
-- Indexes for table `tblproduct`
--
ALTER TABLE `tblproduct`
  ADD PRIMARY KEY (`ProductId`),
  ADD KEY `categoryId` (`categoryId`),
  ADD KEY `tblproduct_ibfk_1` (`companyId`);

--
-- Indexes for table `tblstockin`
--
ALTER TABLE `tblstockin`
  ADD PRIMARY KEY (`StockInId`),
  ADD KEY `CompanyId` (`CompanyId`),
  ADD KEY `tblstockin_ibfk_1` (`ProductId`);

--
-- Indexes for table `tblstockout`
--
ALTER TABLE `tblstockout`
  ADD PRIMARY KEY (`StockOutId`),
  ADD KEY `companyId` (`companyId`),
  ADD KEY `Itemid` (`Itemid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblcategory`
--
ALTER TABLE `tblcategory`
  MODIFY `categoryId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tblcompany`
--
ALTER TABLE `tblcompany`
  MODIFY `CompanyId` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tblproduct`
--
ALTER TABLE `tblproduct`
  MODIFY `ProductId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tblstockin`
--
ALTER TABLE `tblstockin`
  MODIFY `StockInId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tblstockout`
--
ALTER TABLE `tblstockout`
  MODIFY `StockOutId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tblproduct`
--
ALTER TABLE `tblproduct`
  ADD CONSTRAINT `tblproduct_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `tblcompany` (`CompanyId`);

--
-- Constraints for table `tblstockin`
--
ALTER TABLE `tblstockin`
  ADD CONSTRAINT `tblstockin_ibfk_1` FOREIGN KEY (`ProductId`) REFERENCES `tblproduct` (`ProductId`);

--
-- Constraints for table `tblstockout`
--
ALTER TABLE `tblstockout`
  ADD CONSTRAINT `tblstockout_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `tblcompany` (`CompanyId`),
  ADD CONSTRAINT `tblstockout_ibfk_2` FOREIGN KEY (`Itemid`) REFERENCES `tblproduct` (`ProductId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
