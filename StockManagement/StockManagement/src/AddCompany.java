
import Database.CompanyDB;
import Model.Company;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class AddCompany extends javax.swing.JInternalFrame {

    public AddCompany() {
        initComponents();
        formLoad();
        hiddenTextField.setVisible(false);
    }

    public void formLoad() {
        //table  Row remove
        this.setSize(640, 540);
        companyTable.removeColumn(companyTable.getColumnModel().getColumn(3));
        LoadAllCompany();
        updateCompanyBtn.setVisible(false);
        deleteCompanyBtn.setVisible(false);
        resetBtn.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        companyTextArea = new javax.swing.JTextArea();
        companyTextBox = new javax.swing.JTextField();
        companyInfoSaveBtn = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        companyTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        updateCompanyBtn = new javax.swing.JButton();
        deleteCompanyBtn = new javax.swing.JButton();
        resetBtn = new javax.swing.JButton();
        hiddenTextField = new javax.swing.JTextField();

        setBackground(new java.awt.Color(204, 255, 204));
        setClosable(true);
        setTitle("Setup Company Information");
        setPreferredSize(new java.awt.Dimension(744, 650));

        companyTextArea.setColumns(20);
        companyTextArea.setRows(5);
        jScrollPane1.setViewportView(companyTextArea);

        companyInfoSaveBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        companyInfoSaveBtn.setForeground(new java.awt.Color(0, 102, 0));
        companyInfoSaveBtn.setText("Save");
        companyInfoSaveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                companyInfoSaveBtnActionPerformed(evt);
            }
        });

        companyTable.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        companyTable.setForeground(new java.awt.Color(0, 102, 0));
        companyTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Serial", "Company Name", "Company Description", "id"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        companyTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                companyTableMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(companyTable);
        if (companyTable.getColumnModel().getColumnCount() > 0) {
            companyTable.getColumnModel().getColumn(0).setResizable(false);
            companyTable.getColumnModel().getColumn(3).setResizable(false);
        }
        companyTable.getAccessibleContext().setAccessibleDescription("");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 0));
        jLabel1.setText("Company Name :");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 102, 0));
        jLabel2.setText("Company Description : ");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 51));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("  Add A Company Information ");
        jLabel3.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 0, 0)));

        updateCompanyBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        updateCompanyBtn.setForeground(new java.awt.Color(0, 102, 0));
        updateCompanyBtn.setText("Update");
        updateCompanyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateCompanyBtnActionPerformed(evt);
            }
        });

        deleteCompanyBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        deleteCompanyBtn.setForeground(new java.awt.Color(255, 0, 0));
        deleteCompanyBtn.setText("Delete");
        deleteCompanyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteCompanyBtnActionPerformed(evt);
            }
        });

        resetBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        resetBtn.setForeground(new java.awt.Color(255, 0, 0));
        resetBtn.setText("Reset");
        resetBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(109, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(hiddenTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2))))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(companyTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                        .addComponent(resetBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteCompanyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(updateCompanyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(companyInfoSaveBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 421, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(101, 101, 101))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(172, 172, 172))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(companyTextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(68, 68, 68)
                        .addComponent(hiddenTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                    .addComponent(companyInfoSaveBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deleteCompanyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(resetBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(updateCompanyBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(155, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    CompanyDB aCompanyBD = new CompanyDB();

    private void companyInfoSaveBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_companyInfoSaveBtnActionPerformed
        Company aCompany = new Company();
        aCompany.CompanyName = companyTextBox.getText();
        aCompany.CompanyDescription = companyTextArea.getText();

        if (!companyTextBox.getText().isEmpty()) {

            if (!aCompanyBD.IsExistCompany(aCompany)) {
                String message = aCompanyBD.SaveCompany(aCompany);
                JOptionPane.showMessageDialog(null, message, "message", JOptionPane.INFORMATION_MESSAGE);
                LoadAllCompany();
                ClearCompany();
            } else {
                JOptionPane.showMessageDialog(null, "Same Product Already Exists", "Error", JOptionPane.ERROR_MESSAGE);
                ClearCompany();
            }
        } else {
            JOptionPane.showMessageDialog(null, " Company Name Empty  Not Allowed", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_companyInfoSaveBtnActionPerformed

    private void resetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetBtnActionPerformed
        ClearCompany();
        ClearAll();
    }//GEN-LAST:event_resetBtnActionPerformed

    DefaultTableModel model;
    private void companyTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_companyTableMouseClicked
        updateCompanyBtn.setVisible(true);
        deleteCompanyBtn.setVisible(true);
        resetBtn.setVisible(true);
        companyInfoSaveBtn.setVisible(false);

        DefaultTableModel model = (DefaultTableModel) companyTable.getModel();
        int selectedRowIndex = companyTable.getSelectedRow();
        // get selected row data  
        String CompanyName = model.getValueAt(selectedRowIndex, 1).toString();
        String CompanyDesc = model.getValueAt(selectedRowIndex, 2).toString();
        String id = model.getValueAt(selectedRowIndex, 3).toString();

        companyTextBox.setText(CompanyName);
        companyTextArea.setText(CompanyDesc);
        hiddenTextField.setText(id);
    }//GEN-LAST:event_companyTableMouseClicked


    private void updateCompanyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateCompanyBtnActionPerformed
        Company aCompany = new Company();
        aCompany.CompanyID = Integer.parseInt(hiddenTextField.getText());
        aCompany.CompanyName = companyTextBox.getText();
        aCompany.CompanyDescription = companyTextArea.getText();
        boolean result = aCompanyBD.UpdateCompany(aCompany);
        if (result == true) {
            JOptionPane.showMessageDialog(null, "Company Information Update Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
            LoadAllCompany();
            ClearCompany();
            ClearAll();
        } else {
            JOptionPane.showMessageDialog(null, "Company Information Update failed", "Error", JOptionPane.ERROR_MESSAGE);
            ClearCompany();
        }
    }//GEN-LAST:event_updateCompanyBtnActionPerformed

    private void deleteCompanyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteCompanyBtnActionPerformed
        Company aCompany = new Company();
        aCompany.CompanyID = Integer.parseInt(hiddenTextField.getText());
        boolean result = aCompanyBD.DeleteCompany(aCompany);
        if (result == true) {
            JOptionPane.showMessageDialog(null, "Company Information Delete Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
            LoadAllCompany();
            ClearCompany();
            ClearAll();
        } else {
            JOptionPane.showMessageDialog(null, "Company Information Delete failed", "Error", JOptionPane.ERROR_MESSAGE);
            ClearCompany();
        }
    }//GEN-LAST:event_deleteCompanyBtnActionPerformed

    private void LoadAllCompany() {
        DefaultTableModel model = (DefaultTableModel) companyTable.getModel();
        model.setRowCount(0);    //for clear Table Data
        List<Company> LoadAllCompany = aCompanyBD.ViewAllCompany();
        int count = 0;
        for (Company companyes : LoadAllCompany) {
            Company company = new Company();
            ++count;
            company.CompanyID = companyes.CompanyID;
            company.CompanyName = companyes.CompanyName;
            company.CompanyDescription = companyes.CompanyDescription;
            model.addRow(new Object[]{count, company.CompanyName, company.CompanyDescription, company.CompanyID});
        }
    }

    public void ClearCompany() {
        companyTextBox.setText("");
        companyTextArea.setText("");
        hiddenTextField.setText("");
    }

    public void ClearAll() {
        updateCompanyBtn.setVisible(false);
        deleteCompanyBtn.setVisible(false);
        resetBtn.setVisible(false);
        companyInfoSaveBtn.setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton companyInfoSaveBtn;
    private javax.swing.JTable companyTable;
    private javax.swing.JTextArea companyTextArea;
    private javax.swing.JTextField companyTextBox;
    private javax.swing.JButton deleteCompanyBtn;
    private javax.swing.JTextField hiddenTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton resetBtn;
    private javax.swing.JButton updateCompanyBtn;
    // End of variables declaration//GEN-END:variables
}
