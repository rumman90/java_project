
import Database.ProductDB;
import Database.StockInDB;
import Model.StockIn;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;

public class StockInItem extends javax.swing.JInternalFrame {

    public StockInItem() {
        initComponents();
        this.setSize(640, 540);
        viewAllCompany();
    }

    public void viewAllCompany() {
        ProductDB aProductDB = new ProductDB();
        HashMap<String, Integer> mapCompanylist = aProductDB.CompanyView();
        for (String C : mapCompanylist.keySet()) {
            companyCombobox.addItem(C);
        }
    }

    int idCompany = 0;
    int productId = 0;
    StockInDB aStockInDB = new StockInDB();

    public void viewAllProduct() {
        itemNameCombobox.removeAllItems();
        itemNameCombobox.insertItemAt("---Select A Item---", 0);
        itemNameCombobox.setSelectedIndex(0);
        HashMap<String, Integer> mapProductlist;
        mapProductlist = aStockInDB.productView(idCompany);

        for (String d : mapProductlist.keySet()) {
            itemNameCombobox.addItem(d);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        companyCombobox = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        itemNameCombobox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        reorderLevelTextBox = new javax.swing.JTextField();
        availableQuentitytextBox = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        stockIntextBox = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        stockInBtn = new javax.swing.JButton();

        setTitle(" Product Stock IN    ");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 102, 0));
        jLabel4.setText("Company Name :");

        companyCombobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        companyCombobox.setForeground(new java.awt.Color(0, 102, 0));
        companyCombobox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--- Select A Company ---" }));
        companyCombobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                companyComboboxActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Monotype Corsiva", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 102, 51));
        jLabel5.setText(" Product Stock IN    ");
        jLabel5.setBorder(javax.swing.BorderFactory.createMatteBorder(2, 2, 2, 2, new java.awt.Color(0, 0, 0)));

        itemNameCombobox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemNameCombobox.setForeground(new java.awt.Color(0, 102, 0));
        itemNameCombobox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--- Select A Item ---" }));
        itemNameCombobox.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                itemNameComboboxItemStateChanged(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 0));
        jLabel1.setText(" Item Name :");

        reorderLevelTextBox.setEditable(false);
        reorderLevelTextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        reorderLevelTextBox.setForeground(new java.awt.Color(0, 102, 0));

        availableQuentitytextBox.setEditable(false);
        availableQuentitytextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        availableQuentitytextBox.setForeground(new java.awt.Color(0, 102, 0));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 0));
        jLabel6.setText("Reorder Level :");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 102, 0));
        jLabel7.setText("Available Quentity :");

        stockIntextBox.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockIntextBox.setForeground(new java.awt.Color(0, 102, 0));

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 0));
        jLabel8.setText("Product Stock In :");

        stockInBtn.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        stockInBtn.setForeground(new java.awt.Color(0, 102, 0));
        stockInBtn.setText("Stock In Item");
        stockInBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stockInBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(124, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(69, 69, 69)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(stockIntextBox)
                            .addComponent(availableQuentitytextBox)
                            .addComponent(itemNameCombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(companyCombobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(reorderLevelTextBox)))
                    .addComponent(stockInBtn))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(120, 120, 120))
            .addGroup(layout.createSequentialGroup()
                .addGap(222, 222, 222)
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addComponent(jLabel5)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(companyCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(itemNameCombobox, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(reorderLevelTextBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(availableQuentitytextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(stockIntextBox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(stockInBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void companyComboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_companyComboboxActionPerformed
        try {
            ProductDB aProductDB = new ProductDB();
            HashMap<String, Integer> mapCompanylist = aProductDB.CompanyView();
            // jLabel2.setText(mapCompanylist.get(companyCombobox.getSelectedItem().toString()).toString());
            String CompanyId = mapCompanylist.get(companyCombobox.getSelectedItem().toString()).toString();
            idCompany = Integer.parseInt(CompanyId);

            viewAllProduct();
        } catch (Exception e) {
//
        }
    }//GEN-LAST:event_companyComboboxActionPerformed

    private void itemNameComboboxItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_itemNameComboboxItemStateChanged
        try {
            HashMap<String, Integer> mapProductlist = aStockInDB.productView(idCompany);

            if (evt.getStateChange() == ItemEvent.SELECTED) {
                if (this.itemNameCombobox.getSelectedIndex() > 0) {
                    //  jLabel3.setText(mapProductlist.get(itemNameCombobox.getSelectedItem().toString()).toString());
                    String ProductId = mapProductlist.get(itemNameCombobox.getSelectedItem().toString()).toString();
                    productId = Integer.parseInt(ProductId);

                    ArrayList<StockIn> ProductResult = aStockInDB.ShowProductInfo(idCompany, productId);
                    for (StockIn result1 : ProductResult) {
                        String AvailableQuantity = String.valueOf(result1.AvailableQuantity);
                        String reorderLevelText = String.valueOf(result1.ReorderLevel);
                        reorderLevelTextBox.setText(reorderLevelText);
                        availableQuentitytextBox.setText(AvailableQuantity);
                    }
                }
            }
        } catch (Exception e) {
//
        }
    }//GEN-LAST:event_itemNameComboboxItemStateChanged


    private void stockInBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stockInBtnActionPerformed
        StockIn aStockIn = new StockIn();
        StockInDB aStockInDB = new StockInDB();
        try {
            if (idCompany > 0 && productId > 0) {
                if (!stockIntextBox.getText().isEmpty()) {
                    if (Integer.parseInt(stockIntextBox.getText()) >= 0) {
                        int AvailableQ = Integer.parseInt(availableQuentitytextBox.getText());
                        int StockIN = Integer.parseInt(stockIntextBox.getText());
                        AvailableQ = AvailableQ + StockIN;

                        aStockIn.ReorderLevel = Integer.parseInt(reorderLevelTextBox.getText());
                        aStockIn.AvailableQuantity = AvailableQ;
                        aStockIn.StockInQuantity = Integer.parseInt(stockIntextBox.getText());
                        aStockIn.CompanyId = idCompany;
                        aStockIn.ItemId = productId;

                        boolean result = aStockInDB.UpdateAvailableQ(aStockIn);
                        if (result == true) {
                            String message = aStockInDB.addStockIn(aStockIn);
                            JOptionPane.showMessageDialog(null, message, "message", JOptionPane.INFORMATION_MESSAGE);
                            clear();
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, " Stock In Field Negative Value  Not Allowed", "Error", JOptionPane.ERROR_MESSAGE);
                        clear();
                    }
                } else {
                    JOptionPane.showMessageDialog(null, " Stock In Field Empty  Not Allowed", "Error", JOptionPane.ERROR_MESSAGE);
                    clear();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Please Select a Company And Item .", "Message", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            //
        }

    }//GEN-LAST:event_stockInBtnActionPerformed

    public void clear() {
        idCompany = 0;
        productId = 0;
        stockIntextBox.setText("");
        reorderLevelTextBox.setText("");
        availableQuentitytextBox.setText("");
        companyCombobox.setSelectedIndex(0);
        itemNameCombobox.setSelectedIndex(0);

    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField availableQuentitytextBox;
    private javax.swing.JComboBox companyCombobox;
    private javax.swing.JComboBox<String> itemNameCombobox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField reorderLevelTextBox;
    private javax.swing.JButton stockInBtn;
    private javax.swing.JTextField stockIntextBox;
    // End of variables declaration//GEN-END:variables
}
