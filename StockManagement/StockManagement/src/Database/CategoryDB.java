
package Database;
import Model.Category;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class CategoryDB extends ConnectDB {

    public void addCategory(String ReceivedCategoryName) {

        try {
            Query = "INSERT INTO tblCategory (categoryName) VALUES('" + ReceivedCategoryName + "')";
            int count = 0;
            count = stmt.executeUpdate(Query);//insert code here
            if (count > 0) {
                JOptionPane.showMessageDialog(null, "Category Name Insert Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Failed to save Category Data", "Error", JOptionPane.ERROR_MESSAGE);
            }
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void IsCategoryNoExist(String ReceivedCategoryName) {
        try {
            Query = "SELECT * FROM tblCategory WHERE categoryName='" + ReceivedCategoryName + "'";
            ResultSet rs = stmt.executeQuery(Query);
            if (rs.next() == true) {
                JOptionPane.showMessageDialog(null, "Category Data Already Exists", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                addCategory(ReceivedCategoryName);
            }
            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    

    public ArrayList<Category> DataTableShowData() {

        ArrayList<Category> categoryList = new ArrayList<Category>();
        try {
            ResultSet rs = stmt.executeQuery("select * from tblCategory");
            while (rs.next()) {
               Category category = new Category();
                category.Categoryid = rs.getInt(1);
                category.CategoryName = rs.getString(2);
                categoryList.add(category);
            }

            conn.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return categoryList;
    }

}
